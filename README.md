[ ![Download](https://api.bintray.com/packages/mdashlw/maven/mojang-api/images/download.svg) ](https://bintray.com/mdashlw/maven/mojang-api/_latestVersion)
[![pipeline status](https://gitlab.com/mdashlw/mojang-api/badges/master/pipeline.svg)](https://gitlab.com/mdashlw/mojang-api/commits/master)

# Mojang API

Kotlin Mojang API wrapper

## Importing

Replace `VERSION` with the latest version above.

### Gradle Groovy DSL

<details><summary>build.gradle</summary>
<p>

```gradle
repositories {
    jcenter()
}

dependencies {
    implementation 'ru.mdashlw.mojang:mojang-api:VERSION'
}
```

</p>
</details>

### Gradle Kotlin DSL

<details><summary>build.gradle.kts</summary>
<p>

```kotlin
repositories {
    jcenter()
}

dependencies {
    implementation("ru.mdashlw.mojang:mojang-api:VERSION")
}
```

</p>
</details>

### Maven

<details><summary>pom.xml</summary>
<p>

```xml
<depedencies>
    <dependency>
        <groupId>ru.mdashlw.mojang</groupId>
        <artifactId>mojang-api</artifactId>
        <version>VERSION</version>
  </dependency>
</depedencies>

<repositories>
    <repository>
      <id>jcenter</id>
      <name>JCenter</name>
      <url>https://jcenter.bintray.com/</url>
    </repository>
</repositories>
```

</p>
</details>

## Usage

### Methods

All return types are nullable.

UUIDs must be undashed.

#### Getting profile

See [Mojang API wiki](https://wiki.vg/Mojang_API#Username_-.3E_UUID_at_time).

Returns: **Profile**.

```kotlin
MojangApi.retrieveProfile("name")
MojangApi.retrieveProfile("name", 0)
```

#### Getting name history

See [Mojang API wiki](https://wiki.vg/Mojang_API#UUID_-.3E_Name_history).

Returns: **NameHistory**.

```kotlin
MojangApi.retrieveNameHistory("uuid")
```

#### Getting name from uuid

This method uses `retrieveNameHistory` and gets the last element.

Returns: **String**.

```kotlin
MojangApi.retrieveName("uuid")
```

### Entities

#### Profile

Represents a Mojang profile.

| Property |  Type  | Description |
|:--------:|:------:|:-----------:|
|  **id**  | String |     UUID    |
| **name** | String |     Name    |

#### NameHistory

Represents a name history, extends `ArrayList<NameHistory.NameChange>`.

##### NameChange

|     Property    |  Type  |  Description  |
|:---------------:|:------:|:-------------:|
|     **name**    | String |      Name     |
| **changedToAt** |  Long? | Changed to at |

## License

The project is licensed under the **[MIT license](https://choosealicense.com/licenses/mit/)**.
