package ru.mdashlw.mojang.api.entities

class NameHistory : ArrayList<NameHistory.NameChange>() {
    data class NameChange(
        val name: String,
        val changedToAt: Long?
    )
}
