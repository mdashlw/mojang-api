package ru.mdashlw.mojang.api.entities

data class Profile(
    val id: String,
    val name: String
)
