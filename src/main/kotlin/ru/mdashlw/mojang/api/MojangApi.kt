package ru.mdashlw.mojang.api

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import okhttp3.OkHttpClient
import ru.mdashlw.mojang.api.entities.NameHistory
import ru.mdashlw.mojang.api.entities.Profile
import ru.mdashlw.mojang.api.util.newCall
import kotlin.reflect.KClass

object MojangApi {
    const val BASE_URL = "https://api.mojang.com/"

    private val okHttpClient: OkHttpClient = OkHttpClient()
    private val jackson: ObjectMapper = jacksonObjectMapper()
        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

    fun retrieveProfile(name: String, timestamp: Long? = null) =
        get<Profile>("users/profiles/minecraft/$name${timestamp?.let { "?at=$it" }.orEmpty()}")

    fun retrieveNameHistory(uuid: String) =
        get<NameHistory>("user/profiles/$uuid/names")

    fun retrieveName(uuid: String) = retrieveNameHistory(uuid)?.last()?.name

    inline fun <reified T : Any> get(endpoint: String): T? = get(T::class, endpoint)

    fun <T : Any> get(replyClass: KClass<T>, endpoint: String): T? {
        val url = "$BASE_URL$endpoint"

        val response = okHttpClient.newCall(url) ?: return null

        return jackson.readValue(response, replyClass.java)
    }
}
